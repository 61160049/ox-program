import java.util.*;
public class OX {
    public static void main(String args []){
        char [][] table = {{'-', '-', '-'},{'-', '-', '-'},{'-', '-', '-'}};
        char player = 'O';
        showWelcome();
        while(true){
            showTable(table);
            showTurn(player);
            input(table,player);
            if(checkWin(table)){break;}
            if(checkDraw(table)){break;}
            player = switchPlayer(player);}
        showTable(table);
        showWin(player,table);
        showBye();
    }
    public static void showWelcome(){
        System.out.println("Welcome to Ox Game.");
    }
    public static void showTable(char [][] table){
        for(int i = 0; i<3; i++){
            for(int j = 0; j<3; j++){
                System.out.print(table[i][j]);
            }
            System.out.println();
        }
    }
    public static void showTurn(char player){
        if(player=='O'){
            System.out.println("Turn of player O.");
        }else{
            System.out.println("Turn of player X.");
        }
    }
    public static void input(char [][] table,char player){
        Scanner scan = new Scanner(System.in);
        System.out.print("Please input Row and Column. : ");
        try{
            int row = scan.nextInt();
            int col = scan.nextInt();
            if(row>0 && row <4 && col >0 && col <4){
                if(table[row-1][col-1]== '-'){
                    table[row-1][col-1]= player;   
                }else{checkSamePosition(table,player);}
            }else{checkWrongInput(table,player);}
        }catch(Exception e){checkNotNumber(table,player);}
    }
    private static void checkSamePosition(char [][] table,char player) {
        System.out.println("You or Your rival already put it in.");
        System.out.println("Please input again.");
        input(table,player);
    }
    private static void checkWrongInput(char [][] table,char player) {
	System.out.println("Please put the number not more than 3.");
	input(table,player);
    }
    private static void checkNotNumber(char [][] table,char player) {
        System.out.println("Please input only number.");
	input(table,player);
    }
    public static boolean checkWin(char [][] table){
        for (int i = 0; i < 3; i++) {
            if (checkRow(i,table)){
		return true;
            }else if (checkColumn(i,table)) {
		return true;
            }else if (checkCross1(table)) {
		return true;
            }else if (checkCross2(table)) {
		return true;
            }
	}
	return false;
    }
    public static boolean checkRow(int i,char [][] table) {
	if(table[i][0]!='-'){
            if(table[i][0]==(table[i][1]) && table[i][0]==(table[i][2])){
		return true;
            }else{
		return false;
            }
        }else{
            return false;
	}
    }
    public static boolean checkColumn(int i,char [][] table) {
	if (table[0][i]!='-') {
            if(table[0][i]==(table[1][i])&&table[0][i]==(table[2][i])) {
                return true;
            }else {
                return false;
            }
	}else {
            return false;
	}
    }
    public static boolean checkCross1(char [][] table) {
	if (table[1][1]!='-') {
            if (table[0][0]==(table[1][1]) && table[0][0]==(table[2][2])) {
		return true;
            } else {
		return false;
            }
	} else {
            return false;
	}
    }
    public static boolean checkCross2(char [][] table) {
	if (table[1][1]!='-') {
            if (table[0][2]==(table[1][1]) && table[0][2]==(table[2][0])) {
                return true;
            } else {
		return false;
            }
	} else {
            return false;
	}
    }
    public static boolean checkDraw(char [][] table) {
	for(int i = 0;i < 3;i++){
            for(int j = 0;j < 3;j++){
                if(table[i][j] == '-'){
                    return false;
                }
            }
        }
        return true;
    }
    public static char switchPlayer(char player) {
        if (player=='O') {
            return 'X';
        }else{
            return 'O';
        }
    }
    public static void showWin(char player,char [][] table) {
        if(checkWin(table)) {
            System.out.println("Player "+player+" Win.");
	}else if(checkDraw(table)) {
            System.out.println("Draw.");
        }
    }
    public static void showBye() {
        System.out.println("Bye Bye, Sir.");
    }
}